﻿namespace HelixGUI
{
    partial class SimEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.applyButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.daysBox = new System.Windows.Forms.NumericUpDown();
            this.couplesBox = new System.Windows.Forms.NumericUpDown();
            this.sizeBox = new System.Windows.Forms.NumericUpDown();
            this.seedBox = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.daysBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.couplesBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seedBox)).BeginInit();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.okButton.Location = new System.Drawing.Point(136, 278);
            this.okButton.Margin = new System.Windows.Forms.Padding(4);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(100, 28);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cancelButton.Location = new System.Drawing.Point(244, 278);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(100, 28);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(83, 54);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Days:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 86);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Root Couples:";
            // 
            // applyButton
            // 
            this.applyButton.Location = new System.Drawing.Point(28, 278);
            this.applyButton.Margin = new System.Windows.Forms.Padding(4);
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size(100, 28);
            this.applyButton.TabIndex = 6;
            this.applyButton.Text = "Apply";
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(56, 117);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Map Size:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // daysBox
            // 
            this.daysBox.Location = new System.Drawing.Point(136, 54);
            this.daysBox.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.daysBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.daysBox.Name = "daysBox";
            this.daysBox.Size = new System.Drawing.Size(132, 22);
            this.daysBox.TabIndex = 9;
            this.daysBox.Value = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.daysBox.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // couplesBox
            // 
            this.couplesBox.Location = new System.Drawing.Point(136, 86);
            this.couplesBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.couplesBox.Name = "couplesBox";
            this.couplesBox.Size = new System.Drawing.Size(132, 22);
            this.couplesBox.TabIndex = 10;
            this.couplesBox.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // sizeBox
            // 
            this.sizeBox.Location = new System.Drawing.Point(136, 117);
            this.sizeBox.Maximum = new decimal(new int[] {
            512,
            0,
            0,
            0});
            this.sizeBox.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.sizeBox.Name = "sizeBox";
            this.sizeBox.Size = new System.Drawing.Size(132, 22);
            this.sizeBox.TabIndex = 11;
            this.sizeBox.Value = new decimal(new int[] {
            64,
            0,
            0,
            0});
            // 
            // seedBox
            // 
            this.seedBox.Location = new System.Drawing.Point(136, 145);
            this.seedBox.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.seedBox.Minimum = new decimal(new int[] {
            -2147483648,
            0,
            0,
            -2147483648});
            this.seedBox.Name = "seedBox";
            this.seedBox.Size = new System.Drawing.Size(132, 22);
            this.seedBox.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(81, 145);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "Seed:";
            // 
            // SimEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 321);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.seedBox);
            this.Controls.Add(this.sizeBox);
            this.Controls.Add(this.couplesBox);
            this.Controls.Add(this.daysBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.applyButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SimEdit";
            this.Text = "SimEdit";
            this.Load += new System.EventHandler(this.SimEdit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.daysBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.couplesBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seedBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button applyButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown daysBox;
        private System.Windows.Forms.NumericUpDown couplesBox;
        private System.Windows.Forms.NumericUpDown sizeBox;
        private System.Windows.Forms.NumericUpDown seedBox;
        private System.Windows.Forms.Label label4;
    }
}