﻿namespace HelixGUI
{
    partial class Book
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logButton = new System.Windows.Forms.Button();
            this.familyTreeButton = new System.Windows.Forms.Button();
            this.mapButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // logButton
            // 
            this.logButton.Location = new System.Drawing.Point(102, 29);
            this.logButton.Name = "logButton";
            this.logButton.Size = new System.Drawing.Size(75, 23);
            this.logButton.TabIndex = 0;
            this.logButton.Text = "Log";
            this.logButton.UseVisualStyleBackColor = true;
            this.logButton.Click += new System.EventHandler(this.logButton_Click);
            // 
            // familyTreeButton
            // 
            this.familyTreeButton.Location = new System.Drawing.Point(102, 57);
            this.familyTreeButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.familyTreeButton.Name = "familyTreeButton";
            this.familyTreeButton.Size = new System.Drawing.Size(75, 23);
            this.familyTreeButton.TabIndex = 1;
            this.familyTreeButton.Text = "Family Tree";
            this.familyTreeButton.UseVisualStyleBackColor = true;
            this.familyTreeButton.Click += new System.EventHandler(this.familyTreeButton_Click);
            // 
            // mapButton
            // 
            this.mapButton.Location = new System.Drawing.Point(102, 85);
            this.mapButton.Name = "mapButton";
            this.mapButton.Size = new System.Drawing.Size(75, 23);
            this.mapButton.TabIndex = 1;
            this.mapButton.Text = "Map";
            this.mapButton.UseVisualStyleBackColor = true;
            this.mapButton.Click += new System.EventHandler(this.mapButton_Click);
            // 
            // Book
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.familyTreeButton);
            this.Controls.Add(this.mapButton);
            this.Controls.Add(this.logButton);
            this.Name = "Book";
            this.Text = "Book";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button logButton;
        private System.Windows.Forms.Button familyTreeButton;
        private System.Windows.Forms.Button mapButton;
    }
}