﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HelixGUI
{
    public partial class Map : Form
    {

        /* Constants */
        public const int TERRAIN_LAND = 1;
        public const int TERRAIN_WATER = 2;

        private int mouseX;
        private int mouseY;
        private float rectSize;

        Helix.Terrain terrain;

        Rectangle[] landRects;
        Color[] landColors;
            
        public Map(Helix.World world)
        {
            InitializeComponent();
            this.terrain = world.terrain;
            makeRects();
        }

        /// <summary>
        /// Creates the Rectangle[] that represents all of the land
        /// </summary>
        private void makeRects()
        {
            if (terrain == null)
                return;
            List<Rectangle> land = new List<Rectangle>();
            List<Color> colors = new List<Color>();

            /*The number of pixels on each side of a rectangle*/
            rectSize = Math.Min(this.Width - 18, this.Height - 45) / (float)Math.Sqrt(terrain.zones.Length);  //sqrt because it's 2d
            for (int x = 0; x < this.terrain.width; x++)
            {
                for (int y = 0; y < this.terrain.height; y++)
                {
                    Helix.Zone zone = terrain.zones[x, y];   
                    if (zone.type == TERRAIN_LAND)    //It is land
                    {
                        Rectangle rect = new Rectangle((int)(x * rectSize), (int)(y * rectSize), (int)(rectSize + 1), (int)(rectSize + 1)); //Add .9 to the terrainHeight and terrainWidth to prevent awkward gaps
                        land.Add(rect);
                        int relHeight = (terrain.maxHeight - zone.height);
                        Color c = Color.FromArgb(255, 0, 255 - relHeight * 3, 0);
                        colors.Add(c);
                    }
                }
            }

            landRects = land.ToArray();
            landColors = colors.ToArray();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            makeRects();
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            /* Calculate which zone is being hovered on */
            mouseX = Cursor.Position.X - this.Location.X - 9;
            mouseY = Cursor.Position.Y - this.Location.Y - 36;
            int zoneX = (int) ((float) mouseX / rectSize);
            if (zoneX > terrain.width - 1)
                zoneX = terrain.width - 1;
            int zoneY = (int) ((float) mouseY / rectSize);
            if (zoneY > terrain.height - 1)
                zoneY = terrain.height - 1;
            Helix.Zone zone = terrain.zones[zoneX, zoneY];

            /* Display info about said zone */
            label1.Text = String.Format("Zone Selected = ({0}, {1})", zoneX, zoneY);
            label2.Text = (zone.type == TERRAIN_LAND ? "Terrain type: Land" : "Terrain type: Water");
            label3.Text = String.Format("Food: {0}", zone.food);
            label4.Text = String.Format("Fertility: {0}", zone.fertility);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = this.CreateGraphics();
            SolidBrush brush = new SolidBrush(Color.Red);
      
            /* Draw water*/
            brush.Color = Color.Blue;
            g.FillRectangle(brush, 0, 0, Height - 45, Height);

            /* Draw land */
            for (int index = 0; index < landRects.Length; index++)
            {
                brush.Color = landColors[index];
                g.FillRectangle(brush, landRects[index]);
            }

            brush.Dispose();
            g.Dispose();
        }

        private void Map_Load(object sender, EventArgs e)
        {

        }
    }
}
