﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Helix;
using System.Windows.Forms;

namespace HelixGUI
{
    /// <summary>
    /// FamilyTree reads a database and produces a TreeView of all people's heritage.
    /// </summary>
    public partial class FamilyTree : Form
    {
        public FamilyTree()
        {
            InitializeComponent();
        }

        private void FamilyTree_Load(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Load an entire World of people into the TreeView
        /// </summary>
        /// <param name="rootCouples"></param>
        public void LoadPeople(List<Person> rootMales)
        {
            TreeNode rootNode = new TreeNode("World");

            /* Each iteration adds a new root node */
            foreach(Person p in rootMales)
            {
                AddPerson(rootNode, p);
            }

            treeView.Nodes.Add(rootNode);
        }


        /// <summary>
        /// Recursive method to add all people
        /// </summary>
        /// <param name="person">Person to add</param>
        private void AddPerson(TreeNode rootNode, Person person)
        {
            if (person.Gender == Person.GENDER_MALE)
            {
                TreeNode node = new TreeNode(person.Name);

                foreach (Person p in person.Children)
                {
                    AddPerson(node, p);
                }

                rootNode.Nodes.Add(node);
            }
        }
    }
}
