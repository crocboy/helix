﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Helix;

namespace HelixGUI
{
    public partial class Book : Form
    {
        private Simulation sim;

        public Book()
        {
            InitializeComponent();
        }


        public void SetSimulation(Simulation s)
        {
            sim = s;
            this.Text = "Book for Simulation " + s.simId;
        }


        /* Open a Log window */
        private void logButton_Click(object sender, EventArgs e)
        {
            LogWindow window = new LogWindow()
            {
                Text = "Log for Simulation " + sim.simId.ToString()
            };
            window.SetLog(sim.GetWorld().log);
            window.Show();
        }

        private void familyTreeButton_Click(object sender, EventArgs e)
        {
            FamilyTree tree = new FamilyTree()
            {
                Text = "Family Tree for Simulation " + sim.simId.ToString()
            };
            tree.Show();
            tree.LoadPeople(sim.GetWorld().rootMales);
        }

        /* Open a Map window */
        private void mapButton_Click(object sender, EventArgs e)
        {
            Map map = new Map(sim.GetWorld())
            {
                Text = "Map for Simulation " + sim.simId.ToString()
            };
            map.Show();
        }
    }
}
