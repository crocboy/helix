﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Helix;

namespace HelixGUI
{
    public partial class LogWindow : Form
    {
        private Log log;

        public LogWindow()
        {
            InitializeComponent();
        }


        public void SetLog(Log l)
        {
            this.log = l;

            /* Load the mapHeight */
            logBox.Clear();

            foreach(Log.Entry  entry in this.log)
            {
                logBox.AppendText(entry.ToString() + Environment.NewLine);
            }
        }

        private void LogWindow_Load(object sender, EventArgs e)
        {
            searchList.SelectedIndex = 0;
        }

        /* Do a search! */
        private void searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                int searchIndex = searchList.SelectedIndex;

                /* Search by ID */
                if (searchIndex == 0)
                {
                    FillBox(log.GetByID(Convert.ToInt32(searchBox.Text)));
                }

                /* Search by Day */
                else if (searchIndex == 1)
                {
                    FillBox(log.GetByDay(Convert.ToInt32(searchBox.Text)));
                }

                /* Search by Text */
                else if (searchIndex == 2)
                {
                    FillBox(log.GetByText(searchBox.Text));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("You done messed up: \n\n\n" + ex.ToString());
            }
        }


        private void FillBox(List<Log.Entry> entries)
        {
            /* Load the mapHeight */
            logBox.Clear();

            foreach(Log.Entry entry in entries)
            {
                logBox.AppendText(entry.ToString() + Environment.NewLine);
            }
        }
    }
}
