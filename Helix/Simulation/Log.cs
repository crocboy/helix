﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helix
{
    /// <summary>
    /// Log is basically a List tailored to Helix - a special array list that lets us access specific events from the simulation.
    /// </summary>
    public class Log
    {
        public List<Entry> logData = new List<Entry>();
        private World world = null;


        /// <summary>
        /// Constructor for Log class
        /// </summary>
        /// <param name="w">World associated with this Log</param>
        public Log(World w)
        {
            this.world = w;
        }


        /// <summary>
        /// Add a Log entry
        /// </summary>
        /// <param name="mapHeight">String for the Log</param>
        /// <param name="idList">VarArg of people's ID's associated with this message</param>
        public void Add(String tag, String data, params int[] idList)
        {
            Entry e = new Entry()
            {
                Day = this.world.currentDay,
                Tag = tag,
                Data = data
            };

            e.IDList.AddRange(idList);

            logData.Add(e);
        }


        /// <summary>
        /// Return all Entries for the given day
        /// </summary>
        /// <param name="day">Desired day</param>
        /// <returns>List of Entries from the day 'day'</returns>
        public List<Entry> GetByDay(int day)
        {
            List<Entry> retList = new List<Entry>(0);

            foreach (Entry e in logData)
            {
                if (e.Day == day)
                    retList.Add(e);
            }

            return retList;
        }


        /// <summary>
        /// Get all Entries associated with a specific ID
        /// </summary>
        /// <param name="id">Desired ID</param>
        /// <returns>List of all Entry object related to the ID</returns>
        public List<Entry> GetByID(int id)
        {
            List<Entry> retList = new List<Entry>(0);

            foreach (Entry e in logData)
            {
                if (e.IDList.Contains(id))
                    retList.Add(e);
            }

            return retList;
        }


        /// <summary>
        /// Search by text
        /// </summary>
        /// <param name="text">Text to search for</param>
        public List<Entry> GetByText(String text)
        {
            List<Entry> retList = new List<Entry>(0);

            foreach (Entry e in logData)
            {
                if (e.ToString().ToLower().Contains(text.ToLower()))
                    retList.Add(e);
            }

            return retList;
        }


        // Allow us to enumerate over this Log, even though it doesn't extend List<T>
        public IEnumerator<Entry> GetEnumerator() { return logData.GetEnumerator(); }


        /// <summary>
        /// Represents a single Log entry
        /// </summary>
        public class Entry
        {
            public int Day = -1;
            public List<int> IDList = new List<int>(0);
            public String Tag = "";
            public String Data = "";

            public override string ToString()
            {
                return Day.ToString() + ": " + Data;
            }
        }
    }
}
