﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helix
{
    public class Woman : Person
    {
        public Man Spouse = null;
        private bool isBachelor = false;
        public int pregnancy = -1;  //not pregnant

        public Woman(World w, Man dad, Woman mom) : base(w, dad, mom)
        {
            Gender = GENDER_FEMALE;
        }

        /// <summary>
        /// HAVE A CHILLEN
        /// </summary>
        public void PopOneOut()
        {
            //if (Spouse != null) // Only have one if we're married :)
            //{
                int gender = Utility.GetRandomGender();

                if (gender == Person.GENDER_MALE) // Add a boy
                {
                    Man child = new Man(this.world, this.Spouse, this)
                    {
                        Name = Utility.GetRandomName(Person.GENDER_MALE, this.GetLastName())
                    };

                    this.Children.Add(child);
                    this.world.AddPerson(child);
                }
                else  // Add a girl
                {
                    Woman child = new Woman(this.world, this.Spouse, this)
                    {
                        Name = Utility.GetRandomName(Person.GENDER_FEMALE, this.GetLastName())
                    };

                    this.Children.Add(child);
                    this.Spouse.Children.Add(child);
                    this.world.AddPerson(child);
                }

                this.world.log.Add("Birth", String.Format("Child '{0}' born.", this.Children[this.Children.Count - 1].Name), this.ID, this.Spouse.ID, this.Children[this.Children.Count - 1].ID);
            //}
        }

        override public void NextDay()
        {
            base.NextDay(); // Call super method

            /* Put these babes in the market */
            if (Age > Metrics.BACHELOR_START_AGE && !isBachelor)
            {
                world.femaleBachelors.Add(this);
                isBachelor = true;
            }

            if (Age > Metrics.BACHELOR_END_AGE && isBachelor)
            {
                world.femaleBachelors.Remove(this);
                isBachelor = false;
            }

            /* Make babies every once in a while */
            if (Utility.GetRandomInt(10000) == 1 && Spouse != null && pregnancy == -1) // Must be married! and not already pregnant!
            {
                pregnancy = 0;
                world.log.Add("Pregnancy",String.Format("{0} is pregnant with {1}'s child!", this.Name, this.Spouse.Name), this.ID, this.Spouse.ID);
            }

            if (pregnancy != -1)
            {
                pregnancy++;
                //Make a baby if pregnancy is advanced enough
                if (pregnancy > Metrics.GESTATION_PERIOD)
                {
                    PopOneOut();
                    pregnancy = -1;
                }
            }
        }

        public override Dictionary<string, string> GetDBData()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();

            /* Check for valid values in EVERY field */
            data.Add("id", Convert.ToString(this.ID));

            if (Mom == null) // Check Mom
                data.Add("mom", "-1");
            else
                data.Add("mom", Convert.ToString(this.Mom.ID));

            if(Dad == null) // Check Dad
                data.Add("dad", "-1");
            else
                data.Add("dad", Convert.ToString(this.Dad.ID));

            if(Spouse == null) // Check Spouse
                data.Add("spouse", "-1");
            else
                data.Add("spouse", Convert.ToString(this.Spouse.ID));

            if (Name != null && Name != "") // Add name
                data.Add("name", Name);

            data.Add("age", Age.ToString());
            data.Add("life_state", this.LifeState.ToString());
            data.Add("gender", this.Gender.ToString());

            return data;
        }
    }
}
