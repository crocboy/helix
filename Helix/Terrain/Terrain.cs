﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helix
{
    public class Terrain
    {
        /* Constants */
        public const int TERRAIN_LAND = 1;
        public const int TERRAIN_WATER = 2;
        public const float SEA_LEVEL = 100;
        public int maxHeight;

        public int width;
        public int height;

        public Zone[,] zones = null;

        public static Terrain GetNewTerrain(int seed, int w, int h)
        {
            /* Fill the random seed */
            Random rand = new Random(seed);
            byte[] perm = new byte[512];
            rand.NextBytes(perm);

            Terrain t = new Terrain();
            int[,] data = new int[w, h];
            t.zones = new Zone[w, h];
            t.width = w;
            t.height = h;

            data = Noise.getTerrain(w, h, 5, seed);
            t.maxHeight = data.Cast<int>().Max();
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    Zone newZone = new Zone();
                    newZone.height = data[x,y];
                    newZone.type = (data[x,y] > SEA_LEVEL ? TERRAIN_LAND : TERRAIN_WATER);
                    if (newZone.type == TERRAIN_LAND)
                    {
                        newZone.fertility = (float)(t.maxHeight - newZone.height) / 100f;
                        newZone.food = (float)newZone.fertility * ((float)Utility.GetRandomDouble(20) + 90);   //Food is fertility multiplied by a random num from 90-110
                    }
                    else
                    {
                        newZone.fertility = 0;
                        newZone.food = 0;
                    }
                    t.zones[x, y] = newZone; 
                }
            }

            return t;
        }
    }
    /// <summary>
    /// Represents one square on the map and holds information for terrain type, food, soil fertility, etc
    /// </summary>
    public class Zone
    {
        public int type; //Is it land or water?
        public int height; //height of the zone
        public float food; //How much food is available here?
        public float fertility; //At what rate does food replenish? (units of food per day)
    }
}
