﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helix
{
    class Noise
    {

        public static int[,] getTerrain(int width, int height, int roughness, int seed)
        {
            double [,] tempResult = new double[width,height];
            for(int scale = roughness; scale < 8; ++scale)
            {
                int [,] intermediate = generateOneOctave(width, height, seed + scale, 256 >> scale);
                for(int x = 0; x < width; ++x)
                    for(int y = 0; y < height; ++y)
                        tempResult[x,y] += ((double)intermediate[x,y]) / (2 << (scale - roughness));
            }
        
            int [,] result = new int[width,height];
            for(int x = 0; x < width; ++x)
                for(int y = 0; y < height; ++y)
                    result[x,y] = (byte)((int)tempResult[x,y]);
        
        
            return result;
        }


        private static int[,] generateOneOctave(int width, int height, int seed, double scale)
        {
            List<List<Double>> G = new List<List<Double>>();
            Random r = new Random(seed);

            while(G.Count < 256)
            {
                while(true)
                {
                    double first = r.NextDouble() * 2 - 1;
                    double second = r.NextDouble() * 2 - 1;

                    double length = Math.Sqrt(first * first + second * second);
                    if(length < 1.0)
                    {
                        List<Double> newElem = new List<Double>();
                        newElem.Add(first / length);
                        newElem.Add(second / length);
                        G.Add(newElem);
                        break;
                    }
                }
            }
        
            int[] P = new int[256];
            for(int i = 0; i < P.Length; i++)
            {
                P[i] = i;
            }

            for(int i = P.Length - 1; i > 0; i--)
            {
                int index = r.Next(i);
                int temp = P[index];
                P[index] = P[i];
                P[i] = temp;
            }

            int[,] result = new int[width,height];
            for(int x = 0; x < width; ++x)
            {
                for(int y = 0; y < height; ++y)
                {
                    result[x,y] = (int) ((noise(x / scale, y / scale, P, G) + 1) * 128);
                }
            }

            return result;
        }

        private static double drop(double a)
        {
            double b = Math.Abs(a);
            return 1.0 - b * b * b * (b * (b * 6 - 15) + 10);
        }

    
    
        private static double Q(double u, double v)
        {
            return drop(u) * drop(v);
        }

    
    
        private static double dotProduct(List<Double> b, double[] a)
        {
            return a[0] * b[0] + a[1] * b[1];
        }

    
    
        private static double noise(double x, double y, int[] P, List<List<Double>> G)
        {
            double[] cell = new double[] {Math.Floor(x), Math.Floor(y)};

            double sum = 0.0;
            for(int r = 0; r <= 1; ++r)
            {
                for(int s = 0; s <= 1; ++s)
                {
                    double i = cell[0] + r;
                    double j = cell[1] + s;

                    double[] uv = new double[] {x - i, y - j};

                    // try  	
                    int check = (byte) i;
                    int index = P[Math.Abs(check)];
                    index = P[(index + (int) j) % P.Length];
                    List<Double> grad = G[index % G.Count];
                    sum += Q(uv[0], uv[1]) * dotProduct(grad, uv);
                
                   /* } catch(Exception e) 
                   {
                	    System.out.println("ERROR");
                    }*/
                
                }
            }

            return Math.Max(Math.Min(sum, 1.0), -1.0);
        }
    }
}
